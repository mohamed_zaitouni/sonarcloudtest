
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

public class FrameCardinaux extends JFrame {
	private JButton nord,sud,est,ouest ;
	private JLabel centre ;
	private void definirLesElements()
	{
	 nord=new JButton("LILLE") ;
	 sud=new JButton("Marseille") ;
	 est=new JButton("Mulhouse") ;
	 ouest=new JButton("Bretagne") ;
	 centre =new JLabel();
	 getContentPane().add(nord, "North") ;
	 getContentPane().add(sud, "South") ;
	 getContentPane().add(est, "East") ;
	 getContentPane().add(ouest, "West") ;
	 getContentPane().add(centre, "Center") ;
	 
	 nord.addActionListener(new ActionListener() {
		 public void actionPerformed(ActionEvent e)
		 {
			 
			 String a= nord.getText(); 
			 centre.setText(a);
		 }
	 });
	}
	public FrameCardinaux() 
	{
		super("cardinaux");
		getContentPane().setLayout(new BorderLayout());
		definirLesElements();
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE) ;
		setVisible(true) ;
		

	}
}
